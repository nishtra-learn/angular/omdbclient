import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class OmdbSearchServiceService {
  private API_KEY = 'b73693a9';

  constructor(private http: HttpClient) { }

  searchByTitle(title: string) {
    let params = {
      apikey: this.API_KEY,
      t: title
    }
    return this.http.get('http://www.omdbapi.com', { params });
  }
}
