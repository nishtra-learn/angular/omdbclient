export class MovieInfoModel {
    title: string;
    year: string;
    runtime: string;
    director: string;
    actors: string;
}
