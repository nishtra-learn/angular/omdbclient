import { Component, OnInit } from '@angular/core';
import { stringify } from 'querystring';
import { MovieInfoModel } from '../shared/movie-info-model';
import { OmdbSearchServiceService } from '../shared/omdb-search-service.service';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.css']
})
export class SearchFormComponent implements OnInit {
  query: string;
  success: boolean;
  searchResult;

  constructor(private omdbService: OmdbSearchServiceService) { }

  ngOnInit(): void {
  }

  searchOmdb() {
    if (this.query === null)
      return;

    this.omdbService
      .searchByTitle(this.query)
      .subscribe((data) => {
        //console.log(data);

        if (data['Response'] !== 'True') {
          console.log(`Error! ${data['Error']}`);
          this.success = false;
          return;
        }

        this.searchResult = data;
        this.success = true;
      });
  }

  handleSearchFieldKeyPress(e: KeyboardEvent) {
    if (e.key !== 'Enter')
      return;

    this.searchOmdb();
  }

}
